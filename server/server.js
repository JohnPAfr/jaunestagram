import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import Pusher from 'pusher'
import dbModel from './dbModel.js'

/**
 * mongoDB user:
 * admin
 * 7kI5B4Fgow84BNgr
 */

const pusher = new Pusher({
  appId: "1111201",
  key: "f5ab40c91bd47b3c4b4e",
  secret: "1113ff484a8297d84c5f",
  cluster: "eu",
  useTLS: true
});


// Api config
const app = express();
const PORT = process.env.PORT || 3333

// Middlewares
app.use(express.json()) // allow JSON file reading
app.use(cors())


// DB config
const connection_url = 'mongodb+srv://admin:7kI5B4Fgow84BNgr@jaunestagram-cluster.8gidt.mongodb.net/jaunestaDB?retryWrites=true&w=majority';
mongoose.connect(connection_url, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongoose.connection.once('open', () => {
  console.log('DB connected');

  const changeStream = mongoose.connection.collection('posts').watch();

  changeStream.on('change', (change) => {
    console.log('Change triggered on pusher...');
    console.log(change);
    console.log('End of change');

    switch(change.operationType) {
      case "insert":
        console.log('Triggering pusher *** IMG UPLOAD ***');
        const postDetails = change.fullDocument;
        pusher.trigger('posts', 'inserted', {
          caption: postDetails.caption,
          user: postDetails.user,
          image: postDetails.image
        });
        break;
      case "update":
        console.log('Triggering pusher *** POST COMMENT UPDATE ***');
        const update = change.updateDescription;
        pusher.trigger('comments', 'updated');
        break;
      default:
        console.log('Unknown trigger from pusher');
    }

    /* if(change.operationType === 'insert') {
      console.log('Triggering pusher *** IMG UPLOAD ***');
      const postDetails = change.fullDocument;
      pusher.trigger('posts', 'inserted', {
        caption: postDetails.caption,
        user: postDetails.user,
        image: postDetails.image
      });
    } else {
      console.log('Unknown trigger from pusher');
    } */
  })
})

// API routes
app.get('/', (req, res) => res.status(200).send('hello world'));

app.post('/upload', (req, res) => {
  const body = req.body;
  dbModel.create(body, (err, data) => {
    if(err) {
      res.status(500).send(err);
    } else {
      res.status(201).send(data)
    }
  })
});

app.put('/post/comment', (req, res) => {
  const id = req.body.id;
  const body = req.body;

  dbModel.findById(id, (err, data) => {
    data.comment.push({text: body.text, user: body.username});
    data.save();
    if(err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(data)
    }
  });
})

app.get('/comments/:id', (req, res) => {
  const id = req.params.id;
  dbModel.findById(id, (err, data) => {
    if(err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(data.comment)
    }
  });
})

app.get('/sync', (req, res) => {
  dbModel.find((err, data) => {
    if(err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(data)
    }
  })
})

// Listen
app.listen(PORT, () => console.log('Listening on port ' + PORT))