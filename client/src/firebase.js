import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyAb-DTfTIC-71RoD_AGisjopiIkr4WObD4",
  authDomain: "jaunestagram.firebaseapp.com",
  databaseURL: "https://jaunestagram.firebaseio.com",
  projectId: "jaunestagram",
  storageBucket: "jaunestagram.appspot.com",
  messagingSenderId: "789927722737",
  appId: "1:789927722737:web:33c340a467aa38afcff350"
});

const db = firebaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

export { db, auth, storage };
