import React, { useState } from "react";
import { storage } from "./firebase";
import "./ImageUpload.css";
import { Input, Button, Avatar } from "@material-ui/core";
import axios from './axios';

const ImageUpload = ({ username }) => {
  const [image, setImage] = useState(null);
  const [{}, setUrl] = useState("");
  const [progress, setProgress] = useState(0);
  const [caption, setCaption] = useState("");

  const handleChange = (e) => {
    if (e.target.files[0]) {
      setImage(e.target.files[0]);
    }
  };

  const handleUpload = () => {
    const uploadTask = storage.ref(`images/${image.name}`).put(image);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        // progress function ...
        const progress = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setProgress(progress);
      },
      (error) => {
        // Error function ...
        console.log(error);
      },
      () => {
        // complete function ...
        storage
          .ref("images")
          .child(image.name)
          .getDownloadURL()
          .then((url) => {
            setUrl(url);

            // After the image is uploaded to Firebase post it to mongodb
            axios.post('/upload', {
              caption: caption,
              user: username,
              image: url
            })

            // post image inside db
            /* db.collection("posts").add({
              imageUrl: url,
              caption: caption,
              username: username,
              timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            }); */

            setProgress(0);
            setCaption("");
            setImage(null);
          });
      }
    );
  };

  return (
    <div className="imageupload">
      <Avatar
        className="imageupload__avatar"
        alt={username}
        src="/static/images/avatar/1.jpg"
      />
      <div className="imageupload__container">
        <progress className="imageupload__progress" value={progress} max="100" />
        <Input
          placeholder="Enter a caption"
          value={caption}
          onChange={(e) => setCaption(e.target.value)}
        />
        <div className="imageupload__bottom">
          <label htmlFor="imageupload__selectFile">Select a file</label>
          <input type="file" onChange={handleChange} id="imageupload__selectFile" name="selectFile" />
          <Button className="imageupload__button" onClick={handleUpload}>
            Upload
          </Button>
        </div>
      </div>
    </div>
  );
};

export default ImageUpload;
