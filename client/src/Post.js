import React, { useState, useEffect, forwardRef } from "react";
import "./Post.css";
import Avatar from "@material-ui/core/Avatar";
import axios from "./axios";

const Post = forwardRef(
  ({ user, username, postId, imageUrl, caption, pusher }, ref) => {
    const [comments, setComments] = useState([]);
    const [comment, setComment] = useState("");

    const getComments = async () => {
      if (postId) {
        await axios.get(`/comments/${postId}`).then(res => {
          console.log(res)
          setComments(res.data)
        })
      }
    }
    useEffect(() => {
      getComments();
    }, []);

    useEffect(() => {  
      const channel = pusher.subscribe('comments');
      channel.bind('updated', () => {
        getComments()
      })}, [])

    const postComment = async (e) => {
      e.preventDefault();

      await axios.put('/post/comment', {
        id: postId,
        text: comment,
        username: user.displayName
      });
      setComment("");
    };

    return (
      <div className="post" ref={ref}>
        <div className="post__header">
          <Avatar
            className="post__avatar"
            alt={username}
            src="/static/images/avatar/1.jpg"
          />
          <h3>{username}</h3>
        </div>

        <img className="post__image" src={imageUrl} alt="post" />
        <h4 className="post__text">
          {username} <span className="post__caption">{caption}</span>
        </h4>

        <div className="post__comments">
          {comments.map((comment) => (
            <p>
              <b>{comment.user}</b> {comment.text}
            </p>
          ))}
        </div>

        {user && (
          <form className="post__commentBox">
            <input
              className="post__input"
              type="text"
              placeholder="Add a comment..."
              value={comment}
              onChange={(e) => setComment(e.target.value)}
            />
            <button
              disabled={!comment}
              className="post__button"
              type="submit"
              onClick={postComment}
            >
              Post
            </button>
          </form>
        )}
      </div>
    );
  }
);

export default Post;
