import axios from 'axios'

const instance = axios.create({
  //baseURL: "https://jaunestagram.herokuapp.com"
  baseURL: "http://localhost:3333"
});

export default instance;