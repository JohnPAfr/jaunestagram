# Instagram clone - MERN

## Packages

CLIENT

- pusher-js
- image-file-to-base64-exif

SERVER

- express
- mongoose
- pusher
- cors
